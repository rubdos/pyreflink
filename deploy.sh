echo "[pypirc]" > ~/.pypirc
echo "servers = pypi" >> ~/.pypirc
echo "[server-login]" >> ~/.pypirc
echo "username:rubdos" >> ~/.pypirc
echo "password:${PYPI_PASSWORD}" >> ~/.pypirc

python -m pip install --upgrade twine
python -m build
python -m twine upload dist/*.tar.gz
